//
// Created by Helder Paraense on 03/10/2019.
//

#include "entropia.h"

double calcularEntropia(std::vector<double>& probabilidades) {
    double entropia = 0;
    for(auto elem : probabilidades) {
        if(elem == 0) continue;
        entropia += elem*log2(elem);
    }
    return entropia*(-1);
}

double calcularEntropiaTotal(std::map<double, double>& mapaProbabilidadesEntropias) {
    double entropia = 0;
    for(auto elem : mapaProbabilidadesEntropias) {
        entropia += elem.first*elem.second;
    }
    return entropia;
}

