//
// Created by Helder Paraense on 06/10/2019.
//

#ifndef FCM_FCM_H
#define FCM_FCM_H

#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <string>
#include <vector>
#include <string>
#include "entropia.h"


/**
 * @copybrief Lê texto modelo e constroi tabela de contextos
 * @param ficheiro
 * @param kappa
 * @param alfa
 * @return Tabela de Contextos
 */
std::map<std::string, std::map<std::vector<char>, std::vector<double>>> fcm(const std::string& ficheiro,
        const int& kappa, const double& alfa, const bool& caseSensitive);

/**
 * @copybrief Verifica se o caracter atual existe no vector alfabeto
 * @param alfabeto
 * @param caracter
 * @return true se o caracter existe; false se não existe
 */
bool existeNoAlfabeto(const std::vector<char>& alfabeto, char caracter);

/**
 * @copybrief Constroi a tabela de contextos com base no texto modelo
 * @param modelo
 * @param mapaContextos
 * @param mapaContagemEventos
 */
void constroiTabelaDeContextos(std::map<std::string, std::map<std::vector<char>, std::vector<double>>>& modelo,
                            const std::map<std::string, std::vector<char>>& mapaContextos,
                            std::map<std::string, std::vector<double>>& mapaContagemEventos);

/**
 * @copybrief Imprime a tabela de contextos na consola
 * @param modelo
 * @param alfa
 * @param tamanhoAlfabeto
 */
void printTabelaDeContextos(std::map<std::string, std::map<std::vector<char>, std::vector<double>>>& modelo,
                            std::map<std::string, int>& novoMapaOcorrencias,
                            std::map<std::string, double>& mapaContextoEntropia,
                            std::map<std::string, double>& mapaFrequenciaOcorrencias,
                            const double& alfa, int tamanhoAlfabeto);

/**
 * @copybrief Calcula probabilidade de cada elemento da tabela de contextos
 * @param modelo
 * @param alfa
 * @param tamanhoAlfabeto
 * @todo Calcular probabilidade
 */
void probabilidadeDeEventosNoContexto(std::map<std::string, std::map<std::vector<char>,
        std::vector<double>>>& modelo, std::map<std::string,double>& mapaContagemOcorrencias,
        const double& alfa, int tamanhoAlfabeto);

/**
 * @copybrief Calcula probabilidade de cada contexto ocorrer no texto
 * @param mapaContagemAparicoes
 * @todo Calcular probabilidade
 */
std::map<std::string, int> probabilidadeDeOcorrenciasDoContexto(std::map<std::string,double>& mapaContagemAparicoes);

/**
 * @copybrief Calcula a probabilidade de um símbolo ocorrer dado um determinado contexto
 * @param caracterCount
 * @param total
 * @param alfa
 * @param tamanhoAlfabeto
 * @return
 */
double calcularProbabilidade(int caracterCount, int total, const double& alfa, int tamanhoAlfabeto);

std::vector<char> getAlfabeto();

std::map<std::string, std::vector<double>> getMapaFrequenciasAlfabetoPorContexto();

std::map<std::string, double> getMapaFrequenciaOcorrencias();

int getCharCount();

#endif //FCM_FCM_H
