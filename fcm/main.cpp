#include <iostream>
#include <map>
#include <cstring>
#include "fcm.h"
#include "generator.h"

using namespace std;

bool caseSensitive = false;

int main(int argc, char *argv[]) {

    string ficheiro;
    int k;
    double alfa;
    bool generate = false;

    cout << endl;
    if (argc<4){
        cout << "Faltam argumentos" << endl;
        cout << "./[nome_programa] [nome_ficheiro_texto] [kappa] [alfa]" << endl;
        cout << "OU" << endl;
        cout << "./[nome_programa] [nome_ficheiro_texto] [kappa] [alfa] [generate]" << endl;
        return 0;
    } else if(argc > 7) {
        cerr << "Demasiados argumentos";
        return -1;
    }

    for(auto aux = 4; aux < argc; aux++) {
        string test = argv[aux];
        if(argv[aux] == string("case"))
            caseSensitive = true;
        if(argv[aux] == string("generate"))
            generate = true;
    }

    if(!generate)
        cout << "Construir apenas modelo (gerador de texto desabilitado)" << endl;


    ficheiro = argv[1]; // Nome do ficheiro a ler
    k = stoi(argv[2]); // Ordem K do modelo de Markov
    alfa = stod(argv[3]); // Alfa para calculo da entropia de caracteres não presentes no texto

    map<string, map<vector<char>, vector<double>>> modelo = fcm(ficheiro, k, alfa, caseSensitive);


    vector<char> alfabeto = getAlfabeto();
    map<string, double> mapaFrequenciaOcorrencias = getMapaFrequenciaOcorrencias();
    map<string, vector<double>> mapaFrequenciasCaracterePorContexto = getMapaFrequenciasAlfabetoPorContexto();
    int charCount = getCharCount();

    if(generate) {
        cout << "A gerar texto..." << endl;
        string texto = generator(modelo, alfabeto, charCount, mapaFrequenciaOcorrencias, mapaFrequenciasCaracterePorContexto, caseSensitive);
        ofstream myfile;
        string filename = "../GeneratedText_k" + to_string(k) + "_alfa" + to_string(alfa) + "_file.txt";
        myfile.open(filename);
        cout << "Escrevendo texto gerado para arquivo." << endl;
        myfile << texto;
        myfile.close();
    }
    return 0;
}