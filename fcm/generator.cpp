//
// Created by Hell_ on 10/10/2019.
//

#include "generator.h"

using namespace std;

string generator(map<string, map<vector<char>, vector<double>>> &modelo, const vector<char> &alfabeto,
        const int charCount, const map<string, double> &mapaFrequenciasContextos,
        const map<string, vector<double>> &mapaFrequenciasCaracterePorContexto, const bool& caseSensitive) {

    int k = 0;
    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    mt19937 gen(seed);
    // uniform_int_distribution<mt19937::result_type> dist;

    vector<string> primeiraString;
    for (const auto &elemModelo : modelo) {
        if (!k) k = elemModelo.first.size();
        char firstLetter = elemModelo.first[0];
        int asciiFirstLetter = int(firstLetter);
        if (!caseSensitive)
            if (asciiFirstLetter < 97 || asciiFirstLetter > 122)
                continue;
            else
                primeiraString.push_back(elemModelo.first);
        else
            if (asciiFirstLetter < 65 || asciiFirstLetter > 90)
                continue;
            else
                primeiraString.push_back(elemModelo.first);
    }

    if(primeiraString.empty()) {
        int aux;
        vector<string> contextos;
        vector<double> pesosIguais;
        for (auto &elemModelo : mapaFrequenciasContextos) {
            pesosIguais.push_back(elemModelo.second);
            //map<string, double>::const_iterator itr = elemModelo.first.begin();
            contextos.push_back(elemModelo.first);
            //textoGerado += caracterGerado;
        }
        discrete_distribution<int> distribution(pesosIguais.begin(), pesosIguais.end());
        aux = distribution(gen);
        string contextoInicial = contextos[aux];

        primeiraString.push_back(contextoInicial);
    }


    map<int, double> auxIndicesProbabilidades;
    vector<double> pesos;
    int i = 0;
    for (const auto &elem : mapaFrequenciasContextos) {
        if (find(primeiraString.begin(), primeiraString.end(), elem.first) != primeiraString.end())
            pesos.push_back(elem.second);
        i++;
    }

    string textoGerado = gerarPrimeiraString(primeiraString, pesos, gen);

    cout << endl << "Gerador comeca com: " << textoGerado << endl;

    while (textoGerado.length() < charCount) {
        int tamanhoTextoGerado = textoGerado.length();
        string janela = textoGerado.substr(tamanhoTextoGerado - k);
        char caracterGerado;
        for (const auto &elem : mapaFrequenciasCaracterePorContexto) {
            if (elem.first == janela) {
                pesos = elem.second;
                discrete_distribution<int> distribution(pesos.begin(), pesos.end());
                caracterGerado = alfabeto[distribution(gen)];
                textoGerado += caracterGerado;
                break;
            }
        }
        if (textoGerado.length() == tamanhoTextoGerado) {
            vector<double> pesosIguais(alfabeto.size(), 1.0 / alfabeto.size());
            discrete_distribution<int> distribution(pesosIguais.begin(), pesosIguais.end());
            caracterGerado = alfabeto[distribution(gen)];
            textoGerado += caracterGerado;
        }
    }

    return textoGerado;
}

string gerarPrimeiraString(vector<string> primeiraString, vector<double> pesos, mt19937 gen) {
    discrete_distribution<int> distribution(pesos.begin(), pesos.end());

    string textoGerado;
    return primeiraString[distribution(gen)];
}