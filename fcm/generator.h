//
// Created by Hell_ on 10/10/2019.
//

#ifndef FCM_GENERATOR_H
#define FCM_GENERATOR_H


#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <random>
#include <algorithm>
#include <chrono>

/**
 * @copybrief Gerador de texto
 * @param modelo
 * @todo Gerar texto
 */
std::string generator(std::map<std::string, std::map<std::vector<char>, std::vector<double>>> &modelo,
               const std::vector<char> &alfabeto, const int charCount,
               const std::map<std::string, double> &mapaFrequenciasContextos,
               const std::map<std::string, std::vector<double>> &mapaFrequenciasCaracterePorContexto,
               const bool& caseSensitive);
/**
 * @copybrief Gerador de primeira string
 * @param primeiraString
 * @param pesos
 * @param gen
 * @return Primeira string
 */
std::string gerarPrimeiraString(std::vector<std::string> primeiraString, std::vector<double> pesos, std::mt19937 gen);

#endif //FCM_GENERATOR_H
