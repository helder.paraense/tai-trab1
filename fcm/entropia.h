//
// Created by Helder Paraense on 03/10/2019.
//

#ifndef FCM_ENTROPIA_H
#define FCM_ENTROPIA_H

#include <cmath>
#include <vector>
#include <map>

/**
 * @copybrief Calcula entropia dos caracteres do alfabeto
 * @param vetor probabilidades
 * @return entropia de um conjunto de probabilidades
 * @todo Calcular entropia da tabela de contextos
 */
double calcularEntropia(std::vector<double>& probabilidades);

/**
 * @copybrief Calcula entropia total
 * @param mapa de probabilidades de cada contexto e suas entropias
 * @return entropia total do modelo
 * @todo Testar
 */
double calcularEntropiaTotal(std::map<double, double>& mapaProbabilidadesEntropias);


#endif //FCM_ENTROPIA_H
