//
// Created by Helder Paraense on 06/10/2019.
//

#include "fcm.h"

//#define PROBABILITY(occurrences, total, alpha, ALPHABET_LENGTH) ( (occurrences + alpha) / (total + (ALPHABET_LENGTH*alpha)))

using namespace std;

vector<char> vectorAlfabeto;
map<string, vector<double>> mapaFrequenciasAlfabetoPorContexto;
map<string, double> mapaFrequenciaOcorrencias;
int charCount = 0; // Contador de caracteres para calcular entropia

map<string, map<vector<char>, vector<double>>> fcm(const string& ficheiro, const int& kappa, const double& alfa,
        const bool& caseSensitive) {

    /* MAP HERE */
    map<string, map<vector<char>, vector<double>>> modelo;
    map<string, vector<char>> mapaContextos;
    map<string, vector<double>> mapaContagemEventos;
    map<string, double> mapaContextoEntropia;

    ifstream ficheiroLeitura; // Instancia file stream do ficheiro a ler
    ficheiroLeitura.open(ficheiro); //
    cout << "Ficheiro: " << ficheiro << endl;
    cout << "A ler ficheiro" << endl;

    if (ficheiroLeitura) {
        cout << "Ficheiro encontrado" << endl;
    } else {
        cout << "Ficheiro nao encontrado" << endl;
        abort();
    }

    cout << endl << "A criar mapa" << endl;

    char caracter = NULL; // Caracter a recolher do ficheiro
    char proximoCaracter = NULL; // Proximo caracter a recolher do ficheiro
    string caracteres; // String de caracteres para calcular probabilidade de frequencias relativas
    ficheiroLeitura.get(caracter); // Le primeiro caracter
    if(!caseSensitive) caracter = tolower(caracter);
    do {
        if(!existeNoAlfabeto(vectorAlfabeto, caracter)) {
            vectorAlfabeto.push_back(caracter);
        }

        if(kappa == 0) {
            charCount++;
            string aux;
            aux += caracter;
            mapaFrequenciaOcorrencias[aux]++;
        }

        if(kappa > 0) {
            for(int i = 0; caracteres.size() != kappa; i++) {
                caracteres += caracter;
                ficheiroLeitura.get(proximoCaracter);
                if(!caseSensitive) proximoCaracter = tolower(proximoCaracter);
                // while(std::strcmp(&proximoCaracter, "\n") == 0) ficheiroLeitura.get(proximoCaracter);
                charCount++;
                caracter = proximoCaracter;
                if(caracteres.size() == kappa) {
                    mapaFrequenciaOcorrencias[caracteres]++;
                    map<string, vector<char>>::iterator it;
                    it = mapaContextos.find(caracteres);
                    if (it == mapaContextos.end()) {
                        mapaContextos[caracteres].push_back(proximoCaracter);
                        mapaContagemEventos[caracteres].push_back(1);
                    } else {
                        vector<char>::iterator itr;
                        vector<char> auxProximo = mapaContextos[caracteres];
                        itr = find (auxProximo.begin(), auxProximo.end(), proximoCaracter);
                        if (itr != auxProximo.end()) {
                            mapaContagemEventos[caracteres][distance(auxProximo.begin(), itr)]++;
                        } else {
                            mapaContextos[caracteres].push_back(proximoCaracter);
                            mapaContagemEventos[caracteres].push_back(1);
                        }
                    }
                }
            }
            caracteres = caracteres.substr(1);
        }

        if(proximoCaracter == NULL) ficheiroLeitura.get(caracter);
    } while(!ficheiroLeitura.eof());

    ficheiroLeitura.close();
    
    int tamanhoAlfabeto = vectorAlfabeto.size();
    sort(vectorAlfabeto.begin(), vectorAlfabeto.end());
    cout << "_ corresponde a espaço" << endl;
    cout << endl << "Alfabeto de tamanho " << tamanhoAlfabeto << ": [ ";
    for (const auto i: vectorAlfabeto)
        if (i == ' ') {
            string espaco = "_";
            cout << espaco << " ";
        } else if (i == '\n') {
            cout << "/n/ ";
        } else if (i == '\r'){
            cout << "/r/ ";
        } else {
            cout << i << " ";
        }

    cout << "]" << endl;

    cout << "Numero de caracteres do texto: " << charCount << endl;

    if(kappa == 0) {
        double total = 0, probabilidade;
        vector<double> probabilidades;
        for(const auto& elem : mapaFrequenciaOcorrencias) {
                probabilidade = calcularProbabilidade(elem.second, charCount, alfa, tamanhoAlfabeto);
                total += probabilidade;
                cout << elem.first << " " << elem.second << " " << probabilidade << endl;
                probabilidades.push_back(probabilidade);
            }
        cout << "Total de probabilidade: " << total << endl;
        cout << "Entropia: " << calcularEntropia(probabilidades) << endl;
    } else {
        constroiTabelaDeContextos(modelo, mapaContextos, mapaContagemEventos);
        probabilidadeDeEventosNoContexto(modelo, mapaFrequenciaOcorrencias, alfa, tamanhoAlfabeto);
        map<string, int> novoMapaContagemOcorrencias = probabilidadeDeOcorrenciasDoContexto(mapaFrequenciaOcorrencias);
        printTabelaDeContextos(modelo, novoMapaContagemOcorrencias, mapaContextoEntropia, mapaFrequenciaOcorrencias, alfa, tamanhoAlfabeto);
    }

    return modelo;
}

double calcularProbabilidade(int caracterCount, int total, const double& alfa, int tamanhoAlfabeto) {
    double probabilidade = (double) (caracterCount + alfa) / (total + alfa * tamanhoAlfabeto);
    return probabilidade;
}

void constroiTabelaDeContextos(map<string, map<vector<char>, vector<double>>>& modelo, const map<string, vector<char>>& mapaContextos, map<string, vector<double>>& mapaContagemEventos) {
    for(const auto& contexto : mapaContextos) {
        map<vector<char>, vector<double>> mapaContagemSimbolos;
        mapaContagemSimbolos[contexto.second] = mapaContagemEventos[contexto.first];
        modelo[contexto.first] = mapaContagemSimbolos;
    }
}

bool existeNoAlfabeto(const vector<char>& alfabeto, char caracter) {
    for(auto elem : alfabeto) {
        if(caracter == elem) return true;
    }
    return false;
}

void printTabelaDeContextos(map<string, map<vector<char>, vector<double>>> &modelo,
        map<string, int>& novoMapaOcorrencias, map<string, double>& mapaContextoEntropia,
        map<string, double>& mapaFrequenciaOcorrencias, const double& alfa, int tamanhoAlfabeto) {
    string espaco = "_", newElem;
    int auxNumber;
    double total = 0;
    ofstream myfile;
    string filename = "../GeneratedModel.txt";
    myfile.open(filename);
    vector<double> probabilidades(tamanhoAlfabeto);
    for (auto &elem : modelo) {
        for (auto elemModelo : elem.second) {
            auto elemSecond1 = elemModelo.first.begin();
            vector<double>::const_iterator elemSecond2 = elemModelo.second.begin();
            auxNumber = 0;
            for (char i : elem.first) {
                if ( i == ' ') {
                    myfile << espaco;
                } else if (i == '\n') {
                    myfile << "/n/";
                } else if (i == '\r'){
                    myfile << "/r/";
                } else {
                    myfile << i;
                }
            }
            myfile << ": ";
            for (int i = 0; i < tamanhoAlfabeto; i++) {
                auto it = find(elemModelo.first.begin(), elemModelo.first.end(), vectorAlfabeto[i]);
                if(it == elemModelo.first.end()) {
                    probabilidades[i] = (calcularProbabilidade(0, novoMapaOcorrencias[elem.first], alfa, tamanhoAlfabeto));
                } else {
                    int index = distance(elemModelo.first.begin(), it);
                    probabilidades[i] = elemModelo.second[index];
                }
            }
            mapaFrequenciasAlfabetoPorContexto[elem.first] = probabilidades;
            mapaContextoEntropia[elem.first] = calcularEntropia(probabilidades);
            fill(probabilidades.begin(), probabilidades.end(), 0);

            for (&elemSecond1; elemSecond1 < elemModelo.first.end(); elemSecond1++) {
                total += elemSecond2[auxNumber];

                if ( *elemSecond1 == ' ' ) {
                    newElem = "_";
                } else if ( *elemSecond1 == '\n' ) {
                    newElem = "/n/";
                } else if ( *elemSecond1 == '\r' ){
                    newElem = "/r/";
                } else {
                    newElem = *elemSecond1;
                }
//                myfile << newElem << " - " << elemSecond2[auxNumber] << " <-> " << elemSecond2[auxNumber];
                myfile << newElem << " <-> " << elemSecond2[auxNumber];
                auxNumber++;
                if (auxNumber != elemModelo.first.size()) myfile << " | "; else myfile << endl;
            }
        }
    }
    map<double, double> auxMapEntropiaTotal;
    for(auto contexto: mapaContextoEntropia) {
        auxMapEntropiaTotal[contexto.second] = mapaFrequenciaOcorrencias[contexto.first];
    }
    // myfile << "Total de probabilidade: " << total << endl;
    myfile << "Entropia: " << calcularEntropiaTotal(auxMapEntropiaTotal);
    myfile.close();
    cout << "Entropia: " << calcularEntropiaTotal(auxMapEntropiaTotal) << endl;
}

void probabilidadeDeEventosNoContexto(map<string, map<vector<char>, vector<double>>> &modelo, map<string, double>& mapaContagemOcorrencias, const double& alfa, int tamanhoAlfabeto){
    int auxNumber;
    for (auto &elem : modelo) {
        for (auto &elemModelo : elem.second) {
            auto elemSecond1 = elemModelo.first.begin();
            vector<double>::const_iterator elemSecond2 = elemModelo.second.begin();
            auxNumber = 0;
            for (&elemSecond1; elemSecond1 < elemModelo.first.end(); elemSecond1++) {
                elemModelo.second[auxNumber] = calcularProbabilidade(elemSecond2[auxNumber],
                        mapaContagemOcorrencias[elem.first], alfa, tamanhoAlfabeto);
                auxNumber++;
            }
        }
    }
}

map<string, int> probabilidadeDeOcorrenciasDoContexto(map<string, double>& mapaOcorrenciasContexto) {
    double count = 0.0;
    map<string, int> novoMapaOcorrencias;
    for (auto &elem : mapaOcorrenciasContexto)
        count += elem.second;
    for (auto &elem: mapaOcorrenciasContexto) {
        novoMapaOcorrencias[elem.first] = elem.second;
        elem.second = elem.second / count;
    }
    return novoMapaOcorrencias;
}

vector<char> getAlfabeto() {
    return vectorAlfabeto;
}

map<string, vector<double>> getMapaFrequenciasAlfabetoPorContexto() {
    return mapaFrequenciasAlfabetoPorContexto;
}

map<string, double> getMapaFrequenciaOcorrencias() {
    return mapaFrequenciaOcorrencias;
}

int getCharCount() {
    return charCount;
}