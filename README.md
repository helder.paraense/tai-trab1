# Lab Work 1 | Teoria Algoritmica da Informação

## Instruções para rodar o programa "fcm" em Linux
### Requerimentos
- g++

Ou

- cmake (versão 3.14 ou superior)

### Procedimentos
Dentro da pasta "tai-trab1/fcm", onde contém o ficheiro CmakeList, fazer o comando:

#### Opção 1 | Compilar o programa
cmake .

make

#### Opção 2 | Compilar o programa

g++ main.cpp entropia.cpp entropia.h generator.cpp generator.h fcm.h fcm.cpp -o fcm

### Rodar o programa

- [nome_programa]: nome do programa executável (e.g.: fcm)
- [nome_ficheiro_texto]: path do ficheiro (e.g.: ../ficheiro.txt)
- [kappa]: ordem do modelo (e.g.: 2)
- [alfa]: parâmetro de suavização (e.g.: 0.001)
- [generate]: indicador opcional para executar o gerador de texto (valor **generate** para executar)
- [case]: indicador opcional para definir se é case sensitive (valor **case** para case sensitive)

#### Exemplos para executar o programa para construir modelo e gerar texto

##### Caso 1 :: (./[nome_programa] [nome_ficheiro_texto] [kappa] [alfa]):

./fcm ../ficheiro.txt 2 0.001

##### Caso 2 :: (./[nome_programa] [nome_ficheiro_texto] [kappa] [alfa] [generate]):

./fcm ../ficheiro.txt 2 0.001 generate

##### Caso 3 :: (./[nome_programa] [nome_ficheiro_texto] [kappa] [alfa] [case]):

./fcm ../ficheiro.txt 2 0.001 case

##### Caso 4 :: (./[nome_programa] [nome_ficheiro_texto] [kappa] [alfa] [generate] [case]):

./fcm ../ficheiro.txt 2 0.001 generate case

Ou

./fcm ../ficheiro.txt 2 0.001 case generate

##### Notas: 
- a ordem de entrada dos parâmetros opcionais é indiferente
- os ficheiros gerados pelo programa são guardados no diretório raiz

### Comandos úteis (caso necessário)
- Dar permissão de execucao ao arquivo "programa":

sudo chmod 744 programa
